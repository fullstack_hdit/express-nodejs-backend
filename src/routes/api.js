import express from "express";
import apiControler from "../controller/apiControler";
const route = express.Router();
/**
 *
 * @param {*} app : express app
 */
const apiRouter = (app) => {
  //trả về api rest api
  route.get("/test", apiControler.test);
  route.post("/register", apiControler.handleRegister);

  return app.use("/api/v1/", route); //định nghĩa link góc của trang
};
export default apiRouter;
