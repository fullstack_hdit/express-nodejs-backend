import express from "express";
import homeController from "../controller/homeController";
import apiControler from "../controller/apiControler";
const route = express.Router();
/**
 *
 * @param {*} app : express app
 */
const innitWebRouter = (app) => {
  //duyệt qua các route cho res tương ứng
  route.get("/", homeController.handleHelloWorld);
  route.get("/user", homeController.handleUserPage);
  route.post("/user/user-create", homeController.handleUserCreate);
  route.post("/user/user-delete/:id", homeController.handleUserDelete);
  route.get("/user/user-update/:id", homeController.handleGetUserById);
  route.post("/user/user-update", homeController.handleUserUpdate);

  //trả về api
  route.get("/api/test", apiControler.test);

  return app.use("/", route); //định nghĩa link góc của trang
};
export default innitWebRouter;
