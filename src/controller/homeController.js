//chỉ điểu hướng trang mô hình NVC

import userService from "../service/userService.js";

const handleHelloWorld = (req, res) => {
  return res.render("home.ejs");
};

const handleUserPage = async (req, res) => {
  let userList = await userService.getUserList();
  return res.render("user.ejs", { userList });
};

const handleUserCreate = async (req, res) => {
  let email = req.body.email;
  let username = req.body.username;
  let password = req.body.password;
  await userService.createNewUser(email, password, username);
  return res.redirect("/user");
};

const handleUserDelete = async (req, res) => {
  await userService.deleteUser(req.params.id);
  return res.redirect("/user");
};

const handleUserUpdate = async (req, res) => {
  let newEmail = req.body.email;
  let newUserName = req.body.username;
  let id = req.body.id;
  await userService.updateUser(id, newEmail, newUserName);
  return res.redirect("/user");
};

const handleGetUserById = async (req, res) => {
  let user = await userService.getUserById(req.params.id);

  return res.render("user-update.ejs", { user });
};

module.exports = {
  handleHelloWorld,
  handleUserPage,
  handleUserCreate,
  handleUserDelete,
  handleUserUpdate,
  handleGetUserById,
};
