import express from "express";

/**
 *
 * @param {*} app  -express app
 */
const conFigViewEngine = (app) => {
  app.use(express.static("./src/public"));

  //cấu hình cho ứng dụng express
  //ejs là một view engine
  //cấu hình cho đường dẫn lưu các file view
  app.set("view engine", "ejs");
  app.set("views", "./src/views");
};

export default conFigViewEngine; //truyền tham chiếu
