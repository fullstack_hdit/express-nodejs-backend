import mysql from "mysql2";
import bcrypt from "bcryptjs";
import mysqlP from "mysql2/promise";
import db from "../models/index";
import group from "../models/group";
const salt = bcrypt.genSaltSync(10);

const hashPass = (password) => {
  let hashPassword = bcrypt.hashSync(password, salt);
  return hashPassword;
};
const createNewUser = async (email, password, username) => {
  let hashPassword = hashPass(password);
  try {
    await db.User.create({
      userName: username,
      passWord: hashPassword,
      email: email,
    });
  } catch (error) {
    console.log("failed to create a new user");
  }
};

//get user list
const getUserList = async () => {
  try {
    return await db.User.findAll();
  } catch (error) {
    console.log(error);
  }
};

//delete user
const deleteUser = async (id) => {
  // query database
  try {
    await db.User.destroy({
      where: {
        id: id,
      },
    });
  } catch (error) {
    console.log("failed to delete the user with id=", id);
  }
};

// update user in for
const updateUser = async (id, newEmail, newUserName) => {
  console.log("the new usser name:", newUserName);
  try {
    await db.User.update(
      { email: newEmail, userName: newUserName },
      { where: { id: id } }
    );
  } catch (error) {
    console.log("failed to update the user with id=", error);
  }
};

// get user by id
const getUserById = async (id) => {
  try {
    let data = await db.User.findOne({
      where: {
        id: id,
      },
    });
    console.log(data.id);
    // data = data.get({ plain: true }); => convert to js object trường hợp lỗi khi đọc dữ liệu trả về { vì dữ liệu trả về là 1 sequelize object }
    return data;
  } catch (error) {
    console.log(`failed to get user at ${id}`, error);
  }
};
module.exports = {
  createNewUser,
  getUserList,
  deleteUser,
  updateUser,
  getUserById,
};
