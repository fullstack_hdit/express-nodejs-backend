"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    //tạo bảng quan hệ grouprole
    await queryInterface.createTable("GroupRole", {
      groupId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: "group",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      roleId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: "role",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });

    //định nghĩa quan hệ user(1)-(n)group
    await queryInterface.addConstraint("User", {
      fields: ["groupId"],
      type: "foreign key",
      references: {
        table: "group",
        field: "id",
      },
      onDelete: "CASCADE",
      onUpdate: "CASCADE",
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("GroupRole");
    await queryInterface.removeConstraint("Posts", "userId");
  },
};
