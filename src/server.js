import express from "express";
import innitWebRouter from "./routes/web";
import apiRouter from "./routes/api.js";
import conFigViewEngine from "./config/viewEngine";
import bodyParser from "body-parser";
import cors from "./config/cors.js";
import connection from "./config/connect.js";
require("dotenv").config();

const PORT = process.env.PORT || 8080;

const app = express();

//cors middleway
cors(app);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

//cấu hình cho ứng dụng express bao gồm view engine, static file...
conFigViewEngine(app);

//testing connection with DB by sequelize
connection();

//đặt route innit
innitWebRouter(app);
apiRouter(app);

app.listen(PORT, () => {
  console.log("web is listen in PORT =" + PORT);
});
